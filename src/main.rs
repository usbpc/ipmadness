use crate::OutputType::{Decimal, Hexadecimal, Octal};
use itertools::Itertools;
use std::borrow::BorrowMut;
use std::net::Ipv4Addr;
use std::str::FromStr;

struct CombinationsWithRepetition<T: Clone> {
    items: Vec<T>,
    vec: Vec<usize>,
    first: bool
}

impl <T: Clone> CombinationsWithRepetition<T> {
    fn new(n: usize, items: Vec<T>) -> Self {
        let mut vec = Vec::with_capacity(n);
        for i in 0..vec.capacity() {
            vec.push(0)
        }
        Self {
            items,
            vec,
            first: true
        }
    }
}

impl <T: Clone> std::iter::Iterator for CombinationsWithRepetition<T> {
    type Item = Vec<T>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.first {
            self.first = false;
            if self.vec.len() == 0 {
                return None
            }
            let mut ret = Vec::with_capacity(self.vec.len());
            for i in 0..ret.capacity() {
                ret.push(self.items[self.vec[i]].clone())
            }
            return Some(ret)
        }
        let mut i = 0;
        let mut done = false;

        while !done {
            self.vec[i] += 1;
            if self.vec[i] < self.items.len() {
                done = true;
            } else {
                self.vec[i] = 0;
            }
            i += 1;
            if i >= self.vec.len() {
                if self.vec[self.vec.len()-1] == 0 {
                    return None
                }
                break
            }
        }
        if i == self.vec.len() && self.vec[i-1] == self.items.len() {
            return None
        }
        let mut ret = Vec::with_capacity(self.vec.len());
        for i in 0..ret.capacity() {
            ret.push(self.items[self.vec[i]].clone())
        }
        Some(ret)
    }
}

#[derive(Ord, PartialOrd, Eq, PartialEq, Copy, Clone, Debug)]
enum OutputType {
    Decimal,
    Octal,
    Hexadecimal,
}

fn print_as(f: &OutputType, x: u32) {
    match f {
        OutputType::Decimal => print!("{}", x),
        OutputType::Octal => print!("0{:o}", x),
        OutputType::Hexadecimal => print!("0x{:X}", x),
    }
}

fn main() {
    let mut args = std::env::args();
    if args.len() != 2 {
        println!(
            "Invalid arguments! Usage: {} <ip address>",
            args.nth(0).expect("This should never fail!")
        );
        return;
    }

    let ipv4 = Ipv4Addr::from_str(&args.nth(1).expect("This should never fail! (1)")).unwrap();

    let octets = ipv4.octets();

    let mut concat: [u32; 3] = [0; 3];

    concat[0] = ((octets[2] as u32) << 8) + octets[3] as u32;
    concat[1] = ((octets[1] as u32) << 16) + concat[0];
    concat[2] = ((octets[0] as u32) << 24) + concat[1];

    let types = vec![Decimal, Octal, Hexadecimal];
    
    let mut thing = CombinationsWithRepetition::new(0, types.to_vec());

    thing.for_each( |x| println!("{:?}", x));
    
    for i in 0..4usize {
        CombinationsWithRepetition::new(i+1, types.to_vec())
            .for_each(| mut perm| {
                for j in 0..i {
                    print_as(&perm.pop().unwrap(), octets[j].into());
                    print!(".");
                }
                if i <= 2 {
                    print_as(&perm.pop().unwrap(), concat[2 - i]);
                } else {
                    print_as(&perm.pop().unwrap(), octets[3].into())
                }
                print!("\n");
            });
    }
}
